var expect = require('chai').expect;

import { Model } from '../src/model';

class TestModel extends Model<{ fname: string }> {}

describe('Model', function() {
	let m: TestModel;

	beforeEach(function() {
		m = new TestModel({ fname: 'pera' });
	});

	describe('set', function() {
		it('shold set new model value', function() {
			m.set({ fname: 'pera2' });

			expect(m.get('fname')).to.equal('pera2');
		});
	});

	describe('setByKey', function() {
		it('shold set new model value by key', function() {
			m.setByKey('fname', 'pera trta');

			expect(m.get('fname')).to.equal('pera trta');
		});
	});

	describe('get', function() {
		it('shold return model value by key', function() {
			const res = m.get('fname');

			expect(res).to.equal('pera');
		});
	});
});

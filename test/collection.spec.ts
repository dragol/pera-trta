var expect = require('chai').expect;

import { Collection } from '../src/collection';
import { Model } from '../src/model';

class TestModel extends Model<{ fname: string }> {}
class TestCollection extends Collection<TestModel, { fname: string }> {}

describe('Collection', function() {
	let tc: TestCollection;
	beforeEach(function() {
		tc = new TestCollection();
	});

	describe('add', function() {
		it('should add one item to collection', function() {
			tc.add(new TestModel({ fname: 'pera' }));

			expect(tc.getCount).to.be.a('number');
			expect(tc.getCount).to.equal(1);
		});

		it('should add two items to existing collection', function() {
			tc.add(new TestModel({ fname: 'pera1' }));
			tc.add(new TestModel({ fname: 'pera1' }));

			expect(tc.getCount).to.be.a('number');
			expect(tc.getCount).to.equal(2);
		});
	});

	describe('remove', function() {
		it('should remove item from collection', function() {
			const tm = new TestModel({ fname: 'pera' });
			tc.add([ new TestModel({ fname: 'trta' }) ]);
			tc.add(tm);

			tc.remove(tm, 'fname');

			expect(tc.getCount).to.be.a('number');
			expect(tc.getCount).to.equal(1);
			if (tc.getAll()[0]) {
				expect(tc.getAll()[0].get('fname')).to.equal('trta');
			}
		});
	});

	describe('removeAll', function() {
		it('should remove all items from collection', function() {
			tc.add([ new TestModel({ fname: 'pera' }), new TestModel({ fname: 'trta' }) ]);

			tc.removeAll();

			expect(tc.getCount).to.be.a('number');
			expect(tc.getCount).to.equal(0);
		});
	});

	describe('removeAllByKeyValue', function() {
		it('should remove all items from collection', function() {
			tc.add([
				new TestModel({ fname: 'pera' }),
				new TestModel({ fname: 'trta' }),
				new TestModel({ fname: 'trta' })
			]);

			tc.removeAllByKeyValue('fname', 'trta');

			expect(tc.getCount).to.be.a('number');
			expect(tc.getCount).to.equal(1);
			if (tc.getAll()[0]) {
				expect(tc.getAll()[0].get('fname')).to.equal('pera');
			}
		});
	});

	describe('getAll', function() {
		it('should return all items from collection', function() {
			tc.add([ new TestModel({ fname: 'pera1' }), new TestModel({ fname: 'trta' }) ]);

			const res = tc.getAll();

			expect(res.length).to.equal(2);
			expect(res[0].get('fname')).to.equal('pera1');
		});
	});

	describe('getCount', function() {
		it('should return count from collection', function() {
			tc.add([ new TestModel({ fname: 'pera' }), new TestModel({ fname: 'trta' }) ]);

			const res = tc.getCount;

			expect(res).to.equal(2);
		});
	});

	describe('findByKeyValue', function() {
		it('should find one item by key value', function() {
			tc.add([ new TestModel({ fname: 'pera' }), new TestModel({ fname: 'trta' }) ]);

			const res = tc.findByKeyValue('fname', 'trta');

			if (!Array.isArray(res) && res) {
				expect(res.get('fname')).to.equal('trta');
			}
		});

		it('should find two items by key value', function() {
			tc.add([
				new TestModel({ fname: 'pera' }),
				new TestModel({ fname: 'trta' }),
				new TestModel({ fname: 'trta' })
			]);

			const res = tc.findByKeyValue('fname', 'trta');

			if (Array.isArray(res) && res) {
				expect(res.length).to.equal(2);
				expect(res[0].get('fname')).to.equal('trta');
				expect(res[1].get('fname')).to.equal('trta');
			}
		});

		it('should return null when there are no results', function() {
			tc.add([ new TestModel({ fname: 'pera' }) ]);

			const res = tc.findByKeyValue('fname', 'trta');

			expect(res).to.null;
		});
	});

	describe('sortBy', function() {
		it('should sort collection items in ascending order', function() {
			tc.add([ new TestModel({ fname: 'pera' }), new TestModel({ fname: 'trta' }) ]);

			tc.sortBy('fname');

			expect(tc.getAll()[0].get('fname')).to.equal('pera');
			expect(tc.getAll()[1].get('fname')).to.equal('trta');
		});

		it('should sort collection items in descending order', function() {
			tc.add([ new TestModel({ fname: 'pera' }), new TestModel({ fname: 'trta' }) ]);

			tc.sortBy('fname', 'desc');

			expect(tc.getAll()[0].get('fname')).to.equal('trta');
			expect(tc.getAll()[1].get('fname')).to.equal('pera');
		});
	});

	describe('map', function() {
		it('should map all collection items', function() {
			tc.add([ new TestModel({ fname: 'pera' }), new TestModel({ fname: 'trta' }) ]);

			tc.map((el: TestModel) => el.setByKey('fname', el.get('fname').toUpperCase()));

			expect(tc.getAll()[0].get('fname')).to.equal('PERA');
		});
	});
});

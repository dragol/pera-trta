/**
 * Use it to extend your custom model classes
 */
export abstract class Model<T> {
	constructor(private props: T) {}

	/**
	 * Get model prop by key
	 */
	get<K extends keyof T>(key: K): T[K] {
		return this.props[key];
	}

	/**
	 * Update model props
	 */
	set(data: T): void {
		this.props = { ...this.props, ...data };
	}

	/**
	 * Update model prop by key
	 */
	setByKey<K extends keyof T>(key: K, value: T[K]): void {
		this.props[key] = value;
	}
}

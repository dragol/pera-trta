import { Model } from './model';
import { SortOrder } from './sort-order';

/**
 * Use it to extend your custom collection classes
 */
export abstract class Collection<T extends Model<K>, K> {
	private data: T[] = [];

	/**
	 * Add item/item to the collection
	 */
	add(item: T | T[]): void {
		if (Array.isArray(item)) {
			this.data.push(...item);
			return;
		}

		this.data.push(item);
	}

	/**
	 * Remove only first occurance from the collection
	 */
	remove<J extends keyof K>(item: T, byKey: J): boolean {
		const index = this.data.findIndex((i: T) => i.get(byKey) === item.get(byKey));
		this.data.splice(index, 1);
		return index !== -1 ? true : false;
	}

	/**
	 * Remove all items from the collection
	 */
	removeAll(): void {
		this.data = [];
	}

	/**
	 * Remove all items who satisfy key/value relation from the collection 
	 */
	removeAllByKeyValue<J extends keyof K>(key: J, value: K[J]): void {
		const indexForRemove: number[] = [];
		const result: T[] = [];
		this.data.map((el: T, index: number) => {
			if (el.get(key) && el.get(key) === value) {
				indexForRemove.push(index);
			}
		});

		this.data.map((el: T, index: number) => {
			if (!indexForRemove.includes(index)) {
				result.push(el);
			}
		});

		this.data = result;
	}

	/**
	 * Returns whole collection
	 */
	getAll(): T[] {
		return this.data;
	}

	/**
	 * Returns number of items in the collection
	 */
	get getCount(): number {
		return this.data.length;
	}

	/**
	 * Find and return all items in collection that satisfy kay/value relation
	 */
	findByKeyValue<J extends keyof K>(key: J, value: K[J]): null | T | T[] {
		const result = this.data.filter((el: T) => el.get(key) && el.get(key) === value);

		return result.length === 0 ? null : result.length === 1 ? result[0] : result;
	}

	/**
	 * Sort collection by key in ascending or descending fashion
	 */
	sortBy<J extends keyof K>(key: J, order: SortOrder = 'asc') {
		return this.data.sort((a: T, b: T) => {
			const el1 = a.get(key);
			const el2 = b.get(key);

			if (!el1 || !el2) {
				return 0;
			}

			const elementA = typeof el1 === 'string' ? el1.toUpperCase() : el1;
			const elementB = typeof el2 === 'string' ? el2.toUpperCase() : el2;

			let comparison = 0;
			if (elementA > elementB) {
				comparison = 1;
			} else if (elementA < elementB) {
				comparison = -1;
			}

			return order === 'desc' ? comparison * -1 : comparison;
		});
	}

	/**
	 * Calls a defined callback function on each element of the collecction and change it accordingly
	 */
	map(callback: (element: T) => void): void {
		this.data.map((el: T) => {
			return callback(el);
		});
	}
}
